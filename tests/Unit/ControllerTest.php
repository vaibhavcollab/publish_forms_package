<?php

namespace Tests\Unit;

use App\packages\forms\src\controllers\FormSettingController;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Illuminate\Support\Facades\App;
use Illuminate\support\Facades\Request;
use Illuminate\Support\Facades\Input;
// use Laravel\Lumen\Testing\TestCase;
class ControllerTest extends TestCase
{

    public function testIndexMethod(){

        $this->assertDatabaseHas('forms', [
            'title' => 'Master Form',
        ]);

        $this->assertDatabaseHas('form_fields', [
            'name' => 'title',
            'placeholder' => 'Form title',
            'type' => 'text',
            'label' => 'Form title'
        ]);

        $response = $this->call('GET', 'form_setting');
        $response->assertViewHasAll([
            "headers",
            "listingData",
            "paginationData",
            "settings",
            "addEditFormKey",
            "filterFormData"
        ]);
    }

    public function testShowMethod(){

        $this->assertDatabaseHas('forms', [
            'title' => 'Master Form',
        ]);

        $this->assertDatabaseHas('form_fields', [
            'name' => 'title',
            'placeholder' => 'Form title',
            'type' => 'text',
            'label' => 'Form title'
        ]);

        $this->assertDatabaseHas('master_type', [
            'code' => 'STATUS',
            'description' => 'Status'

        ]);

        $this->assertDatabaseHas('report_attribute', [
            'attribute_name' => 'State'
        ]);


    }


    public function testStoreMethod(){

        $request = new \Illuminate\Http\Request();
        $request->setMethod('POST');
        $request->request->add([
            "form_id" => 117,
            "filter_type" => "filter",
            "field_label" => "Eu id aliquam qui sa",
            "field_type" => "month",
            "field_value" => "Officia voluptatem",
            "list_code" => null,
            "db_mapping" => "Aut in dolorem natus",
            "field_classes" => "Rerum quod sed volup",
            "field_name" => "Odessa Silva",
            "attributes" => "Quo dolorum natus ip",
            "field_id" => "Et at accusamus quas",
            "placeholder" => null,
            "sequence" => "57"

         ]);

        $controller = new FormSettingController();
        $response = $controller->store($request);
        $this->assertTrue($response->isRedirection());
        // $response->assertRedirect(url("form_setting/edit"));
        // $response->assertOk();

    }


    public function testEditFormFieldMethod(){

        $request = new \Illuminate\Http\Request();
        $request->setMethod('POST');
        $request->request->add([
             'filter_type' => "filter",
             'field_id' => 117,
        ]);
            $controller = new FormSettingController();
            $response = $controller->editFormField($request);
            $this->assertNotNull($response);
            $fieldDetails='{"id":117,"form_id":16,"name":"approve_name","field_value":"bnmerrors@gmail.com","placeholder":"","field_id":"approve_name","field_classes":"","label":"Approve Name","type":"text","attributes":"required","db_mapping":"approve_name","list_code":"","child_dd_id":"","sequence":4,"parent_id":null,"created_by":0,"updated_by":0,"is_deleted":0,"is_completed":0,"created_at":"0000-00-00 00:00:00","updated_at":"2022-01-22 11:51:35"}';
            //  $this->assertEquals(200, $response->status());
            //  $this->assertTrue($response);
            // $actual=$this->repo->getcomment(1);
              self::assertEquals($response,$fieldDetails);

    }

    public function testDeleteMethod(){

        $request = new \Illuminate\Http\Request();
        $request->setMethod('POST');
        $request->request->add([
             'field_id' => 116
        ]);
            $controller = new FormSettingController();
            $response = $controller->deleteFormField($request);
            $this->assertNull($response);

            $this->assertDatabaseHas('form_fields', [
                'form_id' => 116,
                'name'=>'Baker',
                'is_deleted' => 1
            ]);
    }


    public function testRestoreMethod(){

        $request = new \Illuminate\Http\Request();+
        $request->setMethod('POST');
        $request->request->add([
             'field_id' => 116,
             'name'=>'Baker'
        ]);
            $controller = new FormSettingController();
            $response = $controller->restoreFormField($request);
            $this->assertNull($response);

            $this->assertDatabaseHas('form_fields', [
                'form_id' => 116,
                'name'=>'Jael',
                'is_deleted' => 0
            ]);
            //   $this->assertTrue(assertNull($response));
    }

    public function testUpdateMethod(){

        $request = new \Illuminate\Http\Request();
        $request->setMethod('POST');
        $request->request->add([
            "form_id" => 117,
            "filter_type" => "filter",
            "field_id" => 470,
            "edit_field_label" => "Eu id aliquam qui sa",
            "edit_field_type" => "month",
            "edit_field_value" => "Officia voluptatem",
            "edit_list_code" => null,
            "edit_db_mapping" => "Aut in dolorem natus",
            "edit_field_classes" => "Rerum quod sed volup",
            "edit_field_name" => "Odessa Silva",
            "edit_attributes" => "Quo dolorum natus ip",
            "edit_field_id" => "Et at accusamus quas",
            "edit_placeholder" => null,
            "edit_sequence" => "12",
            "edit_child_dd_id" => null,
            "form_key" => "Obcaecati laudantium"

        ]);
        $controller = new FormSettingController();
            $response = $controller->updateField($request);
            // $response->assertRedirect(url("forms/edit"));
            $this->assertTrue($response->isRedirection());
            // $response->assertOk();
    }


}
