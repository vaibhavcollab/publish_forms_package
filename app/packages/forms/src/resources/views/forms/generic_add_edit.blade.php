<div class="modal-content">
    <div class="modal-header" style="display: block">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">{{$form_data->title}}</h4>
    </div>
    <div class="modal-body" id="modalBody">
        <div class="loading"></div>
        <div class="row">
            <div class="col-md-12" id="errorDiv" style="display:none;">
                <div class="row">
                    <div class="alert alert-danger show" id="errorBody" style="margin-left: 15px;color: red;">
                    </div>
                </div><hr>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <input type="hidden" name="form_action" value="{{$form_data->form_action_route}}">
                <input type="hidden" name="form_key" id="form_key" value="{{$form_data->form_key}}">
                <input type="hidden" name="edit_entry" value="@if( $editEntry != null ){{$editEntry['id']}}@else 0 @endif">

                <form method="post" action="" class="" id="addForm">
                    <div class="">
                        <div class="row">
                            <!-- Render form fields -->
                            @if(isset($field_data) && !empty($field_data))

                            @foreach($field_data as $field)
                                <div class="col-md-{{$columns}}">
                                    @if( $field->type == 'text' || $field->type == 'number')
                                        <div class="form-group ">
                                            <label for="{{$field->field_id}}">{{App\common\Common::get_translation($field->label)}}</label>
                                            @if($field->attributes == 'required')<span class="text-danger font-weight-bolder">*</span>@endif
                                            <input placeholder="{{ $field->placeholder }}" type="{{$field->type}}" class="form-control border-sky-blue {{$field->field_classes}}"
                                                   id="{{$field->field_id}}" {{$field->attributes}} name="{{$field->name}}"

                                            @if( $editEntry != 0 )
                                                value="{{$editEntry[$field->db_mapping]}}"
                                            @else
                                                value="{{$field->field_value}}"
                                            @endif
                                        >
                                        </div>
                                    @endif

                                    @if( $field->type == 'password' || $field->type == 'number')
                                        <div class="form-group ">
                                            <label for="{{$field->field_id}}">{{App\common\Common::get_translation($field->label)}}</label>
                                            @if($field->attributes == 'required')<span class="text-danger font-weight-bolder">*</span>@endif
                                            <input placeholder="{{ $field->placeholder }}" type="{{$field->type}}" class="form-control border-sky-blue {{$field->field_classes}}"
                                                   id="{{$field->field_id}}" {{$field->attributes}} name="{{$field->name}}"

                                            @if( $editEntry != 0 )
                                                value="{{$editEntry[$field->db_mapping]}}"
                                            @else
                                                value="{{$field->field_value}}"
                                            @endif
                                        >
                                        </div>
                                    @endif

                                    @if( $field->type == 'location')
                                        <div class="form-group ">
                                            <!--do not change the id -->
                                            <label for="autocomplete">{{$field->label}}</label>
                                            @if($field->attributes == 'required')<span class="text-danger font-weight-bolder">*</span>@endif
                                            <input type="text" name="{{$field->name}}" id="autocomplete" class="autocomplete form-control border-sky-blue" placeholder="{{ $field->placeholder }}" {{$field->attributes}}
                                            @if( $editEntry != 0 )
                                                value="{{$editEntry[$field->db_mapping]}}"
                                            @else
                                                value=""
                                            @endif
                                        >
                                        <input type="text" name="latitude" id="latitude" class="form-control d-none">
                                        <input type="text" name="longitude" id="longitude" class="form-control d-none">
                                        </div>
                                    @endif

                                    @if( $field->type == 'dropdown' )
                                        <div class="form-group">
                                            <label for="{{$field->field_id}}">{{App\common\Common::get_translation($field->label)}}</label>
                                            @if($field->attributes == 'required')<span class="text-danger font-weight-bolder">*</span>@endif
                                            <select class="form-control border-sky-blue <?php if($field->child_dd_id){ echo 'dependingDropDown';} ?> select-{{ $field->id }}"
                                                    data-id="{{ $field->id }}" data-child-dd_id="{{ $field->child_dd_id  }}" id="{{ $field->field_id }}" name="{{ $field->name }}" {{$field->attributes}}>
                                                <option value="">Select Option</option>
                                                <?php
                                                    $listData = App\common\Common::dropDownList($field->list_code, $field->parent_id);

                                                    if(!empty($listData)) {
                                                    foreach ($listData as $listKey){?>
                                                    <option value="{{ $listKey['value'] }}"
                                                        <?php
                                                        if(!empty($editEntry[$field->db_mapping])) {
                                                            if($editEntry != 0 && $editEntry[$field->db_mapping] == $listKey['value']){
                                                                echo 'selected';
                                                            }
                                                        }
                                                        ?>
                                                        >
                                                        {{ $listKey['label'] }}
                                                    </option>
                                                <?php }#end foreach
                                                } #end if
                                                ?>
                                            </select>
                                        </div>
                                    @endif

                                    @if( $field->type == 'autocomplete_dropdown' )
                                        <div class="form-group">
                                            <label for="{{$field->field_id}}">{{$field->label}}</label>
                                            @if($field->attributes == 'required')<span class="text-danger font-weight-bolder">*</span>@endif
                                            <select class="form-control chosen-select {{$field->field_classes}}" id="firmName" name="firmName">
                                                <option value="">Select</option>
                                                <?php
                                                $listData = App\common\Common::dropDownList($field->list_code, $field->parent_id);
                                                $selected = '';
                                                if($listData){
                                                foreach ($listData as $listKey){
                                                ?>
                                                <option value="{{ $listKey['value'] }}" {{ $selected }}>{{ $listKey['label'] }}</option>
                                                <?php }}?>
                                            </select>
                                        </div>
                                    @endif

                                    @if( $field->type == 'checkbox' && $field->field_id != 'is_completed')
                                        <div class="form-group form-check">
                                            <input type="checkbox" class="form-check-input {{$field->field_classes}}"
                                                id="{{$field->field_id}}"
                                                name="{{$field->name}}"
                                                value="{{$field->field_value}}"
                                                {{$field->attributes}}>

                                            <label class="form-check-label" for="{{$field->field_id}}">{{$field->label}}</label>
                                        </div>
                                    @endif

                                    @if( $field->type == 'radio' )
                                        <?php
                                            $listData = App\common\Common::dropDownList($field->list_code, null);
                                                foreach ($listData as $listKey){?>
                                                    <div class="form-group form-check">
                                                        <input type="radio" class="form-check-input {{$field->field_classes}}"  name="{{$field->name}}"
                                                        value="{{ $listKey['value'] }}"
                                                            <?php if($editEntry != 0 && $editEntry[$field->db_mapping] == $listKey['value'])
                                                            {
                                                                echo 'checked';
                                                            }
                                                            ?>
                                                        >
                                                        {{ $listKey['label'] }}
                                                    </div>
                                        <?php   } ?>
                                    @endif
                                    @if( $field->type == 'date')
                                        <div class="form-group ">
                                            <label for="{{$field->field_id}}">{{$field->label}}</label>
                                            @if($field->attributes == 'required')<span class="text-danger font-weight-bolder">*</span>@endif
                                            <input type="{{$field->type}}" class="form-control border-sky-blue" id="{{$field->field_id}}" {{$field->attributes}} name="{{$field->name}}"

                                            @if( $editEntry != 0 )
                                                value="{{ Carbon\Carbon::parse($editEntry[$field->db_mapping])->format('yy-m-d') }}"
                                            @else
                                                value=""
                                            @endif
                                            >
                                        </div>
                                    @endif
                                    @if( $field->type == 'email')
                                        <div class="form-group ">
                                            <label for="{{$field->field_id}}">{{$field->label}}</label>
                                            @if($field->attributes == 'required')<span style="color: red;font-weight: bolder;">*</span>@endif
                                            <input type="{{$field->type}}" placeholder="{{ $field->placeholder }}" class="form-control border-sky-blue {{$field->field_classes}}" id="{{$field->field_id}}" {{$field->attributes}} name="{{$field->name}}"

                                            @if( $editEntry != 0 )
                                                value="{{ $editEntry[$field->db_mapping] }}"
                                            @else
                                                value=""
                                            @endif
                                            >
                                        </div>
                                    @endif
                                </div>

                                @if( $field->type == 'textarea' )
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="{{$field->field_id}}">{{$field->label}}</label>
                                        @if($field->attributes == 'required')<span class="text-danger font-weight-bolder">*</span>@endif
                                        <textarea name="{{$field->name}}" id="{{$field->field_id}}" class="col-md-12 border-sky-blue {{$field->field_classes}}" {{$field->attributes}}>@if(isset($editEntry[$field->db_mapping])){{$editEntry[$field->db_mapping]}}@endif</textarea>
                                    </div>
                                </div>
                                @endif
                            @endforeach
                            @endif
                        </div>
                    </div>
                    @if( $field->type == 'checkbox' && $field->field_id == 'is_completed')
                    <!-- Swapnil 12-Jan-2021 Jira SMWE-56 -- Added to hide fields for customer frontend view. -->
                        @if(isset($sessionData['company_id']) && $sessionData['company_id'] < 1000)
                        <div class="custom-control custom-switch">
                            <input type="checkbox" name="isCompleted" class="custom-control-input" id="customSwitches" @if(isset($editEntry['is_completed']) && $editEntry['is_completed'] == 1){{ 'checked' }}@endif> <!-- checked -->
                            <label class="custom-control-label" for="customSwitches">Is Completed</label>
                        </div>
                        @endif
                    @endif
                </form>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <button type="button" class="btn btn-outline-sky-blue float-right" tabindex="-1" data-dismiss="modal">Cancel</button>
                @if( $form_data->is_save_button == 1 )
                    <button type="submit" class="btn btn-sky-blue float-right" id="saveButton">Save</button>
                @endif
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {

        var config = {
            '.chosen-select'           : {},
            '.chosen-select-deselect'  : {allow_single_deselect:true},
            '.chosen-select-no-single' : {disable_search_threshold:10},
            '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
        }

        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }

        $(".chosen-select").chosen().change(function() {
            $(this).val()
        });
    });
    $(".allownumericwithoutdecimal").on("keypress keyup blur",function (event) {
        //this.value = this.value.replace(/[^0-9\.]/g,'');
        //$(this).val($(this).val().replace(/[^0-9\.]/g,''));
        if ((event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
    });

    $('.dependingDropDown').on('change', function() {

        var id                  = $(this).val().toString();
        var child_dd_id         = $(this).data('child-dd_id');
        var childStr            = child_dd_id.toString();
        var str = '';
        var indexOf_child_id    = childStr.indexOf(',');

        console.log('indexOf_child_id', indexOf_child_id);

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        if(indexOf_child_id != -1){
            var split_child_id = child_dd_id.split(",");
            $.each( split_child_id, function( key, value ) {

                $.ajax({
                    type: 'GET',
                    url: APP_URL + '/depending_dropdown',
                    data: { 'id' : id, child_dd_id : value },
                    success: function(ret_data) {
                        str = '';
                        if(ret_data.length > 0){

                            $.each(ret_data, function (key, val) {
                                str += '<option value="' + val['value'] + '">' + val['label'] + '</option>';
                            });

                            $('.select-'+value).html(str);
                            $('.select-'+value).trigger("chosen:updated");
                            console.log(str);
                        }else{
                            $('.select-'+value).html(str);
                            $('.select-'+value).trigger("chosen:updated");
                        }
                    }
                });
            });
        }else{
            $.ajax({
                type: 'GET',
                url: APP_URL + '/depending_dropdown',
                data: { 'id' : id, child_dd_id : child_dd_id },
                success: function(ret_data) {

                    if(ret_data.length > 0){
                        str = '';
                        $.each(ret_data, function (key, val) {
                            str += '<option value="' + val['value'] + '">' + val['label'] + '</option>';
                        });

                        $('.select-'+child_dd_id).html(str);
                        $('.select-'+child_dd_id).trigger("chosen:updated");
                        console.log(str);
                    }else{
                        $('.select-'+child_dd_id).html(str);
                        $('.select-'+child_dd_id).trigger("chosen:updated");
                    }
                }
            });
        }



    });

    $('#saveButton').on('click',function(e) {
        e.preventDefault();
        $('.loading').show();
        $('#saveButton').text("Saving...");
        var urlParameter = window.location.search.substr(1).split("&");
        accordianUrlId = urlParameter[0].split('=')[1];
        console.log(accordianUrlId);


        if(isFormValid() == 1){
            var form_action = $('input[name="form_action"]').val();
            var edit_id = $('input[name="edit_entry"]').val();
            var formKey = $('#form_key').val();

            formData = $('#addForm').serializeArray();
            $.ajaxSetup({
                headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            var params = '';
            if(edit_id != 0){
                params = 'type=update&key='+formKey+'&id='+edit_id+'&list_id='+accordianUrlId;
            }
            else {
                params = 'type=insert&key='+formKey+'&id=0&list_id='+accordianUrlId;
            }

            if(form_action != ''){
                $.ajax({
                    type    : 'POST',
                    url     : APP_URL+'/'+form_action+'?'+params,
                    data    : formData,
                    success : function (returnData) {
                        console.log('ret',returnData);
                        if( returnData.error == "false" || returnData.error == false ) {
                            $('#saveButton').text("Saved");
                            $('#modalAddEdit').modal('toggle');
                            location.reload();
                        }else{
                            $('#errorDiv').show();
                            $("#errorBody").text(returnData.msg);
                        }
                    },
                    error   : function(e) {
                        console.log('error-',e)
                    }
                });
            }
        }
        //location.reload();
        $('#saveButton').text("Save");
        $('.loading').hide();
    });

    function isFormValid() {
        var is_validated = 1;
        var error_text = '';
        $('form#addForm').find('input').each(function(){
            if( $(this).prop('required') && $(this).val() == ''){
                $label = $('label[for="'+ $(this).attr('id') +'"]');
                if ($label.length > 0 ) {
                    error_text = error_text+$label[0].innerText + " is required field."+"<br>";
                }
                is_validated = 0;
            }
        });

        $('form#addForm').find('select').each(function(){
            if( $(this).prop('required') && $(this).val() == '') {
                $label = $('label[for="'+ $(this).attr('id') +'"]');
                if ($label.length > 0 ) {
                    error_text = error_text+$label[0].innerText + " is required field."+"<br>";
                }
                is_validated = 0;
            }
        })


        if(is_validated == 0){

            $('#errorDiv').show();
            $('#errorBody').html(error_text);
        }
        return is_validated;
    }
</script>

{{-- javascript code for location drop down dont change name of #autocomplete  --}}

   <script>
       $(document).ready(function() {
            $("#lat_area").addClass("d-none");
            $("#long_area").addClass("d-none");
       });
   </script>

<style>
    .pac-container {
        z-index:  9999999 !important;
    }
</style>

   <script type="text/javascript">

    // console.log('reloading file');
    $.getScript("/js/common.js", function(data, textStatus, jqxhr) {});
    $.getScript("/js/bootstrap.min.js", function(data, textStatus, jqxhr) {});
    </script>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" ></script>
    {{-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" ></script> --}}

