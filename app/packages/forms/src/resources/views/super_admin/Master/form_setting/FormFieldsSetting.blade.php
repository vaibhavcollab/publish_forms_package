@extends('forms::layouts.app')

@section('content')
<div class="container main-body">
    <div class="row">
        <div class="col-md-12">
            <?php if (Session::has('success')) : ?>
                <div class="alert alert-success">
                    <?php echo (Session::get('success')); ?>

                </div>
            <?php endif; ?>
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Filter Fields</a>
                </li>
               <li class="nav-item">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Group Fields</a>
                </li>
                {{--<li class="nav-item">
                    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Filters</a>
                </li>--}}
            </ul>
            <div class="tab-content pt-1" id="myTabContent">
                <!-- Form Fields -->
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">

                    <!-- Button trigger modal -->
                    <button type="button" class="btn btn-sm btn-sky-blue float-right" data-toggle="modal" data-target="#modalPopUpSetting"> Add Fields </button>
                    <button class="btn btn-sm btn-sky-blue float-right mr-1" data-toggle="modal" data-target="#modalPreview" id="btnPreview" >Preview</button>
                    <a href="{{ url('form_setting') }}">
                        <span class="h6 font-weight500 text-orange" style="vertical-align: -webkit-baseline-middle;"> Filter fields for {{ $formsData->title }}</span>
                    </a>
                    <!-- Modal -->

                    <div class="table-responsive accordion-list">
                        <table class="table table-border text-center mt-1">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Label</th>
                                    <th>Name</th>
                                    <th>DB Mapping</th>
                                    <th>Type</th>
                                    <th>Field Value</th>
                                    <th>Placeholder</th>
                                    <th>Field ID</th>
                                    <th>Attributes</th>
                                    <th>Sequence</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody id="general_mst_data">
                                @foreach($formFieldsData as $fields)
                                <tr>
                                    <td>{{ $fields->id }}</td>
                                    <td>{{ $fields->label }}</td>
                                    <td>{{ $fields->name }}</td>
                                    <td>
                                        <span data-toggle="tooltip" data-placement="top" title="<?php echo ($fields->db_mapping); ?>">
                                            <?php echo (substr($fields->db_mapping, 0, 12)); ?>
                                        </span>
                                    </td>
                                    <td>{{ $fields->type }}</td>
                                    <td>{{ $fields->field_value }}</td>
                                    <td>{{ $fields->placeholder }}</td>
                                    <td>
                                        <span data-toggle="tooltip" data-placement="top" title="<?php echo ($fields->field_id); ?>">
                                            <?php echo (substr($fields->field_id, 0, 12)); ?>
                                        </span>
                                    </td>
                                    <td>{{ $fields->attributes }}</td>
                                    <td>{{ $fields->sequence }}</td>
                                    <td class="padding7px">
                                        <button type="button" class="btn btn-sm table-row-btn btnEditFields" id="editFormFields" data-toggle="modal" data-target="#formFieldEdit" data-filter_type="filter" data-fieldId="{{ $fields->id }}">
                                            <span class="fa fa-edit"></span>
                                        </button>
                                        @if($fields->is_deleted == 0)
                                            <button type="button" class="btn btn-sm table-row-btn btnDeleteFields" id="deleteFormFields" data-fieldId="{{ $fields->id }}" data-formId="{{ $form_id  }}">
                                                <span class="fa fa-trash-o"></span>
                                            </button>
                                        @else
                                            <button type="button" class="btn btn-sm table-row-btn btnRestoreFields" id="restoreFormFields" data-fieldId="{{ $fields->id }}" data-formId="{{ $form_id  }}">
                                                <span class="fa fa-refresh"></span>
                                            </button>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-sm-10">
                            <div class="no_of_record">
                                <span>Showing {{$formFieldsData->firstItem()}} to {{$formFieldsData->lastItem()}} of {{$formFieldsData->total()}}</span>
                                <span>Page of {{$formFieldsData->firstItem()}}</span>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <div id="">
                                {{ $formFieldsData->appends($_GET)->links() }}
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Add Group Setting -->
                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                    <!-- Button trigger modal -->
                    <button type="button" class="btn-sm btn-sky-blue fa-pull-right mt-1" data-toggle="modal" data-target="#modalAddGroup">Add Group</button>
                    <span class="h6 font-weight500 text-orange"> Group Filter list for {{ $formsData->title }}</span>
                    <!-- Modal -->
                    <div class="modal fade" id="modalAddGroup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Add Group Fields</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form id="" role="form" method="post" action="{{ route('form_setting.store') }}">
                                        @csrf
                                        <input type="hidden" value="group" name="filter_type">
                                        <input type="hidden" value="{{ $form_id }}" name="form_id">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="txtColumnName">Group Attribute Name</label>
                                                    <input type="text" name="attribute_name" class="form-control border-sky-blue" id="attribute_name" required>
                                                </div>
                                                <button type="reset" class="btn-sm btn-outline-sky-blue fa-pull-right">Reset</button>
                                                <button type="submit" class="btn-sm btn-sky-blue fa-pull-right mr-1">Save</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="modalEditGroup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Edit Group Fields</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form id="" role="form" method="post" action="update">
                                        @csrf
                                        <input type="hidden" value="group" name="filter_type">
                                        <input type="hidden" value="{{ $form_id }}" name="form_id">
                                        <input type="hidden" id="old_group_field_id" value="" name="attribute_id">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label for="txtColumnName">Group Attribute Name</label>
                                                    <input type="text" name="edit_attribute_name" class="form-control border-sky-blue" id="editAttribute_name" required>
                                                </div>
                                                <button type="reset" class="btn-sm btn-outline-sky-blue fa-pull-right">Reset</button>
                                                <button type="submit" class="btn-sm btn-sky-blue fa-pull-right mr-1">Save</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-border  mt-1">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Attribute Name</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody id="general_mst_data">
                            @if(isset($reportGroupData) && !empty($reportGroupData))
                                @foreach($reportGroupData as $attribute)
                                    <tr>
                                        <td> {{ $attribute->id }}</td>
                                        <td> {{ $attribute->attribute_name }}</td>
                                        <td class="padding7px">
                                            <button type="button" class="btn btn-sm table-row-btn btnEditFields" id="editGroupFields" data-toggle="modal" data-target="#modalEditGroup" data-filter_type="group" data-fieldId="{{ $attribute->id }}">
                                                <span class="fa fa-edit"></span>
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- Edit Group Setting -->
                <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                    <!-- Button trigger modal -->
                    <button type="button" class="btn-sm btn-sky-blue fa-pull-right mt-1" data-toggle="modal" data-target="#modalFilterSetting">Add Filter</button>
                    <!-- Modal -->
                    <div class="modal fade" id="modalFilterSetting" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Add Filter</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form action="">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input type="checkbox" name="chkL1" id="chkL1">
                                                            <label for="chkL1">Form Field</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input type="text" name="txtChkL1" class="form-control border-sky-blue">
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-group">
                                                            <input type="checkbox" name="chkL1" id="chkL1">
                                                            <label for="chkL1">Daterange</label>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input type="text" name="txtChkL1" class="form-control border-sky-blue">
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <button type="reset" class="btn-sm btn-outline-sky-blue fa-pull-right">Reset</button>
                                                        <button type="submit" class="btn-sm btn-sky-blue fa-pull-right mr-1">Save</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-border  mt-1">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Column Name</th>
                                    <th>Alias Name</th>
                                    <th>Sequence Number</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Add Form Fields -->
<div class="modal fade" id="modalPopUpSetting" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Form Fields
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </h5>

            </div>
            <div class="modal-body">
                <form id="" role="form" method="post" action="{{ route('form_setting.store') }}">
                    @csrf
                    <input type="hidden" value="filter" name="filter_type">
                    <input type="hidden" value="{{ $form_id }}" name="form_id">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="txtLabelName">Field Label</label>
                                <input type="text" name="field_label" class="form-control border-sky-blue" id="field_label" required>
                            </div>
                            <div class="form-group">
                                <label for="ddlType">Field Type</label>
                                <select class="form-control border-sky-blue field_type" id="field_type" name="field_type" required>
                                    <option value="">-Select--</option>
                                    <option value="text">Textbox</option>
                                    <option value="password">Password</option>
                                    <option value="checkbox">Checkbox</option>
                                    <option value="radio">Radio</option>
                                    <option value="dropdown">Dropdown</option>
                                    <option value="location">Location</option>
                                    <option value="autocomplete_dropdown">Autocomplete Dropdown</option>
                                    <option value="textarea">Textarea</option>
                                    <option value="single_date">Single Date Picker</option>
                                    <option value="date">Date Picker</option>
                                    <option value="month">Month Picker</option>
                                    <option value="file">File</option>
                                    <option value="email">Email</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="txtFieldValue">Field Value</label>
                                <input type="text" name="field_value" class="form-control border-sky-blue" id="field_value">
                            </div>
                            <div class="form-group fieldListCode" id="fieldListCode" style="display: none;">
                                <label for="ddlListCode">List Code</label>
                                <select class="form-control border-sky-blue" id="list_code"  name="list_code">
                                    <option value="">-Select--</option>
                                    @foreach($listCodeData as $listKey)
                                        <option value="{{ $listKey->code }}">{{ $listKey->code }} - {{ $listKey->description }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="txtDBMapping">Db Mapping</label>
                                <input type="text" name="db_mapping" class="form-control border-sky-blue" id="db_mapping" required>
                            </div>
                            <div class="form-group">
                                <label for="txtSequence">Field Classes</label>
                                <input type="text" name="field_classes" class="form-control border-sky-blue" id="field_classes">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="txtName">Field Name</label>
                                <input type="text" name="field_name" class="form-control border-sky-blue" id="field_name" required>
                            </div>
                            <div class="form-group">
                                <label for="txtAttributes">Field Attributes</label>
                                <input type="text" name="attributes" class="form-control border-sky-blue" id="attributes">
                            </div>
                            <div class="form-group">
                                <label for="txtFieldID">Field Id</label>
                                <input type="text" name="field_id" class="form-control border-sky-blue" id="field_id">
                            </div>
                            <div class="form-group fieldPlaceholder" style="display: none;" id="fieldPlaceholder">
                                <label for="txtPlaceholder">Field Placeholder</label>
                                <input type="text" name="placeholder" class="form-control border-sky-blue" id="placeholder">
                            </div>
                            <div class="form-group">
                                <label for="txtSequence">Field Sequence Number</label>
                                <input type="number" name="sequence" class="form-control border-sky-blue" id="sequence" required>
                            </div>
                            <div class="form-group" id="child_dd" style="display: none;">
                                <label for="edit_ddlListCode">Child Drop Down</label>
                                <div class="form-group">
                                    <select class="form-control chosen-select"  id="child_dd_id" name="child_dd_id[]" multiple="">
                                        <option value="">-Select--</option>
                                        @foreach($child_FieldData as $fieldKey)
                                            <option value="{{ $fieldKey->id }}">{{ $fieldKey->label }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button type="reset" class="btn-sm btn-outline-sky-blue fa-pull-right">Reset</button>
                            <button type="submit" class="btn-sm btn-sky-blue fa-pull-right mr-1">Save</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Edit Form Fields -->
<div class="modal fade" id="formFieldEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Form Fields <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </h5>
            </div>
            <div class="modal-body">
                <form id="" role="form" method="post" action="update">
                    @csrf
                    <input type="hidden" value="filter" name="filter_type">
                    <input type="hidden" value="{{ $form_id }}" name="form_id">
                    <input type="hidden" id="old_field_id" value="" name="field_id">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="txtLabelName">Field Label</label>
                                <input type="text" name="edit_field_label" class="form-control border-sky-blue" id="edit_field_label" required>
                            </div>
                            <div class="form-group">
                                <label for="ddlType">Field Type</label>
                                <select class="form-control border-sky-blue" id="edit_field_type" name="edit_field_type" required>
                                    <option value="">-Select--</option>
                                    <option value="text">Textbox</option>
                                    <option value="password">Password</option>
                                    <option value="checkbox">Checkbox</option>
                                    <option value="radio">Radio</option>
                                    <option value="dropdown">Dropdown</option>
                                    <option value="location">Location</option>
                                    <option value="autocomplete_dropdown">Autocomplete Dropdown</option>
                                    <option value="textarea">Textarea</option>
                                    <option value="single_date">Single Date Picker</option>
                                    <option value="date">Date Picker</option>
                                    <option value="month">Month Picker</option>
                                    <option value="file">File</option>
                                    <option value="email">Email</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="txtFieldValue">Field Value</label>
                                <input type="text" name="edit_field_value" class="form-control border-sky-blue" id="edit_field_value">
                            </div>
                            <div class="form-group edit_fieldListCode" id="edit_fieldListCode" style="display: none;">
                                <label for="ddlListCode">List Code</label>
                                <select class="form-control border-sky-blue" id="edit_list_code" name="edit_list_code">
                                    <option value="">-Select--</option>
                                    @foreach($listCodeData as $listKey)
                                        <option value="{{ $listKey->code }}">{{ $listKey->code }} - {{ $listKey->description }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="txtDBMapping">Db Mapping</label>
                                <input type="text" name="edit_db_mapping" class="form-control border-sky-blue" id="edit_db_mapping" required>
                            </div>
                            <div class="form-group">
                                <label for="txtSequence">Field Classes</label>
                                <input type="text" name="edit_field_classes" class="form-control border-sky-blue" id="edit_field_classes">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="txtName">Field Name</label>
                                <input type="text" name="edit_field_name" class="form-control border-sky-blue" id="edit_field_name" required>
                            </div>
                            <div class="form-group">
                                <label for="txtAttributes">Field Attributes</label>
                                <input type="text" name="edit_attributes" class="form-control border-sky-blue" id="edit_attributes">
                            </div>
                            <div class="form-group">
                                <label for="txtFieldID">Field Id</label>
                                <input type="text" name="edit_field_id" class="form-control border-sky-blue" id="edit_field_id">
                            </div>
                            <div class="form-group fieldPlaceholder" id="edit_fieldPlaceholder">
                                <label for="txtPlaceholder">Field Placeholder</label>
                                <input type="text" name="edit_placeholder" class="form-control border-sky-blue" id="edit_placeholder">
                            </div>
                            <div class="form-group">
                                <label for="txtSequence">Field Sequence Number</label>
                                <input type="number" name="edit_sequence" class="form-control border-sky-blue" id="edit_sequence" required>
                            </div>
                            <div class="form-group" id="edit_child_dd" style="display: none;">
                                <label for="ediT_ddlListCode">Child Drop Down</label>
                                <select class="form-control border-sky-blue" id="edit_child_dd_id"  name="edit_child_dd_id">
                                    <option value="">-Select--</option>
                                    @foreach($child_FieldData as $fieldKey)
                                        <option value="{{ $fieldKey->id }}">{{ $fieldKey->label }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button type="reset" class="btn-sm btn-outline-sky-blue fa-pull-right">Reset</button>
                            <button type="submit" class="btn-sm btn-sky-blue fa-pull-right mr-1">Save</button>
                        </div>
                    </div>
                    <input type="hidden" name="form_key" id="form_key" value="{{$formsData->form_key}}">
                </form>
            </div>
        </div>
    </div>
</div>

<!-- preview modal -->
<div id="modalPreview" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg modalPreviewClass" id="modalContent">

        </div>
    </div>

<script>
    $(document).ready(function() {
        $("#btnPreview").click(function() {
            var formKey = $('#form_key').val();
            $.ajax({
                type    : 'GET',
                url     : APP_URL+'/generate_form?key='+formKey+'&id=0',
                success : function (data) {
                    $('.modalPreviewClass').empty();
                    $('.modalPreviewClass').append(data);

                    modalDataBindings = 1;
                    $("#saveButton").hide();
                }
            });
        });
    });

    $('#field_type').on('change', function() {
        var field_type = $('#field_type').val();

        $('#fieldListCode').hide();
        $("#list_code").removeAttr("required","false");
        $('#fieldPlaceholder').hide();
        $('#child_dd').hide();

        if (field_type == 'dropdown') {
            $('#fieldListCode').show();
            $('#child_dd').show();
            $("#list_code").attr("required", "true");
        }

        if (field_type == 'text' || field_type == 'textarea') {
            $('#fieldPlaceholder').show();
        }
    });

    /*----- Edit Change type -----*/
    $('#edit_field_type').on('change', function() {
        var field_type = $('#edit_field_type').val();

        $('#edit_fieldListCode').hide();
        $('#edit_fieldPlaceholder').hide();
        $('#edit_child_dd').hide();

        if (field_type == 'dropdown') {
            $('#edit_fieldListCode').show();
            $('#edit_child_dd').show();
            $("#edit_fieldListCode").attr("required", "true");
        }

        if (field_type == 'text' || field_type == 'textarea') {
            $('#edit_fieldPlaceholder').show();
        }
    });

    $('.btnEditFields').on('click', function() {
        var field_id = $(this).attr("data-fieldid");
        var filter_type = $(this).attr("data-filter_type");
        $('#edit_fieldPlaceholder').hide();
        $('#edit_fieldListCode').hide();
        $('#edit_child_dd').hide();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            type: 'POST',
            url: APP_URL + '/editFormField',
            data: {
                'field_id': field_id, 'filter_type': filter_type
            },
            success: function(ret_data) {

                var data = JSON.parse(ret_data);
                console.log(data);
                $('#old_field_id').val(data.id);
                $('#edit_field_label').val(data.label);
                $('#edit_field_name').val(data.name);
                $('#edit_field_type').val(data.type);
                $('#edit_attributes').val(data.attributes);
                $('#edit_field_value').val(data.field_value);
                $('#edit_field_id').val(data.field_id);
                $('#edit_db_mapping').val(data.db_mapping);
                $('#editAttribute_name').val(data.attribute_name);
                $('#old_group_field_id').val(data.id);

                if (data.type == 'text' || data.type == 'textarea') {

                    $('#edit_fieldPlaceholder').show();
                    $('#edit_placeholder').val(data.placeholder);
                }
                if (data.type == 'dropdown' || data.type == 'radio') {


                    $('#edit_fieldListCode').show();
                    $('#edit_child_dd').show();
                    $('#edit_list_code').val(data.list_code);
                    $('#edit_child_dd_id').val(data.child_dd_id);
                }

                $('#edit_field_classes').val(data.field_classes);
                $('#edit_sequence').val(data.sequence);
            }
        });
    });

    /* Delete Form Field */
    $(function() {
        $('.btnDeleteFields').click(function(e) {
            e.preventDefault();
            var field_id = $(this).attr("data-fieldid");
            var form_id = $(this).attr("data-formId");
            var ok = confirm("Do you really want to Delete?");
            if (ok == true){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'POST',
                    url: APP_URL + '/deleteFormField',
                    data: {
                        'field_id': field_id, 'form_id' : form_id
                    },
                    success: function(ret_data) {
                        window.location.reload();
                    }
                });
            }
        });
    });

    /* Restore Form Field */
    $(function() {
        $('.btnRestoreFields').click(function(e) {
            e.preventDefault();
            var field_id = $(this).attr("data-fieldid");
            var form_id = $(this).attr("data-formId");
            var ok = confirm("Do you really want to Restore?");
            if (ok == true){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'POST',
                    url: APP_URL + '/restoreFormField',
                    data: {
                        'field_id': field_id, 'form_id' : form_id
                    },
                    success: function(ret_data) {
                        window.location.reload();
                    }
                });
            }
        });
    });
</script>
@endsection
