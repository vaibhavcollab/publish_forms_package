<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'CRM') }}</title>

    <!-- Scripts -->
    {{--<script src="{{ asset('js/app.js') }}" defer></script>--}}

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <link href="{{ asset('css/menu.css') }}" rel="stylesheet" />

    {{--<script src="{{ asset('js/general_master.js') }}"></script>--}}
    <script src="{{ asset('js/common.js') }}"></script>
    <script src="{{asset('js/jquery.nestable.js')}}"></script>
    <script src="{{ asset('js/menu.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/orange_theme.css') }}" rel="stylesheet" />


    {{-- Multi select dropdown js / css --}}
    <link href="{{ asset('css/chosen.css') }}" rel="stylesheet">
    <script src="{{asset('js/chosen.jquery.js')}}"></script>
    <link href="{{ asset('fonts/font-awesome.min.css') }}" rel="stylesheet">

    {{-- Date picker js/ css--}}
    <script src="{{ asset('js/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/daterangepicker.min.js') }}" type="text/javascript"></script>
    <link href="{{ asset('css/daterangepicker.css') }}" rel="stylesheet" type="text/css" />

    {{-- accordion js/css--}}
    <link type="text/css" href="{{ asset('css/accordion.css') }}" rel="stylesheet" />
    <script src="{{ asset('js/accordion.js') }}" type="text/javascript"></script>
    <script>
        var APP_URL = "<?php echo url('/'); ?>";
    </script>
    <script src="{{ asset('js/tinymce.min.js') }}" referrerpolicy="origin"></script>

    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" ></script> --}}

    {{-- Location --}}
    <!-- <script src="https://maps.google.com/maps/api/js?key=AIzaSyBLo2emU_Wmv90fXMRdlAmoi500Mk0Ke-4&libraries=places" type="text/javascript"></script> -->

    {{-- Login & Register css--}}
    <!-- <link href="{{ asset('/public-ui/css/custom.css') }}" rel="stylesheet"> -->
</head>
<script>
    // $(document).ready(function() {

    //     $('.alert-success').fadeIn().delay(10000).fadeOut(); //10 sec.

    //     /* Verify otp and login */
    //     $('#verify-otp').on('click', function() {
    //         var user_id = $("#uer_id").val();
    //         $('#alert').text('');
    //         $.ajax({
    //             type: 'GET',
    //             url: APP_URL + '/verifyOTP',
    //             data: {
    //                 'userID': user_id,
    //                 'OTP': $('#OTP').val()
    //             },
    //             success: function(data) {
    //                 console.log(data);

    //                 if (data['STATUS'] == 1) {
    //                     $('#verifyOTP').hide();
    //                     $('#changePassword').show();
    //                 } else {
    //                     if (data['STATUS'] == 0) {
    //                         $('#alert').text('Oppps!.. OTP is not correct!!');
    //                     }
    //                 }
    //             }
    //         });
    //     });
    //     /* Verify otp and login */
    //     $('#change_password').on('click', function() {
    //         var user_id = $("#uer_id").val();
    //         var password = $("#new_password").val();
    //         var conf_password = $("#conf_password").val();

    //         $('#alert').text('');

    //         if (password == conf_password) {
    //             $.ajax({
    //                 type: 'GET',
    //                 url: APP_URL + '/change_password',
    //                 data: {
    //                     'userID': user_id,
    //                     'password': password
    //                 },
    //                 success: function(data) {
    //                     console.log(data);

    //                     alert(data['msg']);
    //                     window.location = APP_URL + '/login';
    //                 }
    //             });
    //         } else {
    //             $('#password_msg').text('Oppps!.. Password not match!!');
    //         }
    //     });

    //     /*-- Set Menu Name in local Storage --*/
    //     $('.navbar-link-color').on('click', function() {
    //         localStorage.setItem('Menu', $(this).data('menu'));
    //         localStorage.setItem('Sub-Menu', '');
    //     });
    //     $('.dropdown-item, .add-submenu-item').on('click', function() {
    //         localStorage.setItem('Sub-Menu', $(this).data('submenu'));
    //     })
    // });
</script>

<body class="hold-transition sidebar-mini">

    <div id="app">
        <nav class="navbar sticky-top navbar-expand-lg bg-light navbar-light pt-0 pb-0 shadow-sm main-nav">
            <!--<div class="container-fluid"> -->
            <a class="navbar-brand" href="#">{{ env('APP_NAME') }}</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav">
                    @guest

                    @else
                    {!! App\common\Common::superAdminMenus() !!}
                    @endguest
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('login') }}">{{ __('Login') }}</a>
                    </li>
                    @if (Route::has('register'))
                    <li class="nav-item">
                        <a class="nav-link" href="{{ url('register') }}">{{ __('Register') }}</a>
                    </li>
                    @endif
                    @else
                    @php $notificationDetails = App\Http\Controllers\HomeController::getBasicNotificationDetails(); @endphp
                    <li class="nav-item dropdown no-arrow mx-1">
                        <a class="nav-link dropdown-toggle nav-item-notification" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fa fa-bell fa-fw"></i>
                            @if($notificationDetails['count'] != 0) <span class="badge badge-danger badge-counter">{{$notificationDetails['count']}}</span>@endif
                        </a>

                        <div class="dropdown-list dropdown-menu dropdown-menu-right notification-width">
                            <h6 class="dropdown-header nav-item-notification-header"> Alerts Center </h6>
                            @if($notificationDetails['count'] != 0)

                            @foreach($notificationDetails['data'] as $notification)
                            <a class="dropdown-item d-flex align-items-center @if($notification['status'] == 0) unread-notification @endif" href="#">
                                <div class="mr-3">
                                    <div class="icon-circle"> <i class="fa fa-user"></i> </div>
                                </div>
                                <div>
                                    <div class="small text-gray-500">{{$notification['created_at']}}</div>
                                    <span class="@if($notification['status'] == 0) font-weight-bold @endif">{!!$notification['notification']!!}</span>
                                </div>
                            </a>
                            @endforeach

                            @else

                            <span class="d-flex align-items-center no-recent-notification" href="#">
                                No Recent Notifications
                            </span>
                            @endif

                            <a class="dropdown-item text-center small nav-item-show-all" href="{{url('/').'/my-notifications'}}" onclick="localStorage.clear();">Show All</a>

                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link navbar-link-color dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            @if (auth()->user()->profile_pic)
                            <img src="{{ File::exists(base_path('public/' . auth()->user()->profile_pic))? url(auth()->user()->profile_pic): url('public-ui/images/profile.png') }}" style="width: 24px; height: 24px; border-radius: 50%;">
                            @endif
                            {{ Auth::user()->name }}
                        </a>

                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="{{ url("user_profile") }}">
                                Profile
                            </a>
                            <?php
                            $session_data = session()->get('user_info');
                            if ($session_data['company_id'] > 1000) {
                            ?>
                                <a class="dropdown-item" href="{{ url("dashboard/supply/list") }}">
                                    Dashboard
                                </a>
                            <?php
                            }
                            ?>
                            <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();localStorage.clear();
                                                     document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
                    @endguest
                </ul>

            </div>
            <!-- </div>-->
        </nav>

        <main class="py-4">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 p-0">
                        @if(Session::has('failed'))
                        <div class="alert alert-danger">
                            {{Session::get('failed')}}
                        </div>
                        @endif
                    </div>
                    <div class="col-md-12 p-0">
                        @if(Session::has('success'))
                        <div class="alert alert-success">
                            {{Session::get('success')}}
                        </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="loading"></div>
            <div style="margin-top: -15px !important;"></div>
            @yield('content')
        </main>
    </div>
    <script>
        $(document).ready(function() {
            $("#allSearch").click(function() {
                $("#search-box").val('');
            });

            $(".navbar-link-color").click(function() {
                $(this).prev(".br-right").addClass("active");
            })
        });
    </script>
    <script type="text/javascript">
        $(document).ready(function() {
            $("#lat_area").addClass("d-none");
            $("#long_area").addClass("d-none");
        });

        google.maps.event.addDomListener(window, 'load', initialize);
        initialize();

        function initialize() {
            var options = {
                componentRestrictions: {
                    country: "IN"
                }
            };
            geocoder = new google.maps.Geocoder();
            console.log(options);
            var input = document.getElementById('autocomplete');
            var autocomplete = new google.maps.places.Autocomplete(input, options);
            autocomplete.addListener('place_changed', function() {
                var place = autocomplete.getPlace();
                $('#latitude').val(place.geometry['location'].lat());
                $('#longitude').val(place.geometry['location'].lng());
            });
        }

        function errorFunction() {
            alert("Geocoder failed");
        }
    </script>
</body>

</html>
