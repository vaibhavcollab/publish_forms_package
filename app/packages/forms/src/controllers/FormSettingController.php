<?php

namespace App\packages\forms\src\controllers;

use App\common\Common;
use App\Exports\Export;
use App\Http\Controllers\Controller;
use App\packages\forms\src\models\MasterModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Exception;
use App\Modules\Auth\Models\User;
use Illuminate\Support\Facades\Session;
use phpDocumentor\Reflection\Types\Array_;

class FormSettingController extends Controller
{

    public function index(Request $request)
    {
        $requestData        = $request->all();
        // dd($requestData);
        $sessionData        = $request->session()->get('user_info');
        $masterModel        = new MasterModel();

        try {
            $addEditFormKey = 'forms';
            $filterFormData                 = DB::table('forms')->where('form_key', '=', 'main_master_filter')->first();
            $settings = [
                'action_edit_button'        => '1',
                'action_view_button'        => '1',
                'action_accordion_button'   => '0',
                'accordion_url'             => '',
                'add_button'                => '1',
                'pagination'                => '1',
                'filter_button'             => '0',
                'group_button'              => '0',
                'export_button'             => '0',
                'import_button'             => '0',
                'search'                    => '1',
                'list_title'                => 'Master Form',
                'action_header'             => '1',
                'multiSaveKey'              => '',
            ];
            $headers = [
                'id'                        => 'Sr. No',
                'title'                     => 'Title',
                'display_columns'           => 'Columns',
                'form_key'                  => 'Form Key',
                'form_action_route'         => 'Action Route',
                'is_save_button'            => 'Save',
                'action_edit_button'        => 'Edit',
                'add_button'                => 'Add',
                'pagination'                => 'Pagination',
                'filter_button'             => 'Filter',
                'export_button'             => 'Export',
                'import_button'             => 'Import',
                'action_view_button'        => 'View'
            ];

            $filterFields    = DB::table('form_fields')->where('form_id', $filterFormData->id)->orderBy('sequence', 'asc')->get();

            $query = DB::table('forms')->select('id','title','display_columns','form_key','form_action_route','is_save_button','action_edit_button','add_button',
                'pagination','filter_button','export_button','import_button','action_view_button');

            if (isset($requestData['basics']) && !empty($requestData['basics'])) {
                $query->where(function ($query) use ($requestData) {
                    $query->where('title', 'LIKE', '%' . $requestData['basics'] . '%')
                        ->orwhere('forms.id', 'LIKE', '%' . $requestData['basics'] . '%')
                        ->orwhere('form_key', 'LIKE', '%' . $requestData['basics'] . '%')
                        ->orwhere('form_action_route', 'LIKE', '%' . $requestData['basics'] . '%');
                });
            }

            if (isset($requestData['filter_column_name']) && isset($requestData['sorting_method']) && !empty($requestData['filter_column_name']) && !empty($requestData['sorting_method'])) {
                $query      = $query->orderBy($requestData['filter_column_name'], $requestData['sorting_method']);
            } else {
                $query      = $query->orderBy('id', 'DESC');
            }

            $export = new Export();
            $sql_query = $export->eloquentSqlWithBindings($query);
            $export->setExcelParameters($sql_query, $headers, $settings['list_title']);

            $paginationData = $query->paginate(10);
            $listingData = json_decode(json_encode($paginationData), true);

            if ($request->ajax()) {
                return view('forms::layouts.listing', compact('headers', 'listingData', 'paginationData', 'settings', 'addEditFormKey', 'filterFormData', 'filterFields'));
            }
        } catch (\Illuminate\Database\QueryException $ex) {
            // $common     = new Common();
            // $common->error_logging($ex, 'index', 'FormSettingController.php');
            // return view('errors.oh!');
            dd($ex->getMessage());
            dd($ex->getLine());
        }

        $filterFormData = '';
        $filterFields = '';

        return view('forms::layouts.listing-container', compact('headers', 'listingData', 'paginationData', 'settings', 'addEditFormKey', 'filterFormData', 'filterFields'));
    }

    /*
     @ Show Form Field listing view.
     */
    public function show(Request $request){
        $requestData = $request->all();

        try {
            $form_id = $requestData['id'];
            $formsData          = DB::table('forms')->where('id', $form_id)->first();
            $formFieldsData     = DB::table('form_fields')->where('form_id', $form_id)
                                ->orderBy('sequence', 'DESC')->paginate(100);

            $listCodeData       = DB::table('master_type')->select('id', 'code', 'description')->orderBy('code')->get();
            $child_FieldData    = DB::table('form_fields')->select('id', 'label')
                ->where('form_id', $form_id)->where('type', 'dropdown')->orderBy('id', 'DESC')->get();

            $reportGroupData    = DB::table('report_attribute')->where('report_id', $form_id)->orderBy('id', 'DESC')->paginate(10);

            if ($request->ajax()) {
                return view('forms::super_admin.Master.form_setting.FormFieldsSetting', compact( 'reportGroupData', 'formsData', 'formFieldsData', 'form_id', 'listCodeData', 'child_FieldData'));
            }
            return view('forms::super_admin.Master.form_setting.FormFieldsSetting', compact('reportGroupData', 'formsData', 'formFieldsData', 'form_id', 'listCodeData', 'child_FieldData'));
        } catch (\Illuminate\Database\QueryException $ex) {
            // $common     = new Common();
            // $common->error_logging($ex, 'show', 'FormSettingController.php');
            // return view('errors.oh!');
            dd($ex->getMessage());
            dd($ex->getLine());
        }

    }

    /*
    @ Store form field.
    */
    public function store(Request $request){
        // dd($request);
        $requestData = $request->all();
        // dd($requestData);
        $masterModel = new MasterModel();

        try {
            if($requestData['filter_type'] == 'group_filter'){
                $insertField['report_id']           = $requestData['form_id'];
                $insertField['attribute_name']      = $requestData['attribute_name'];

                $masterModel->insertData($insertField,'report_attribute');
            }else if($requestData['filter_type'] == 'filter'){

                $insertField['form_id']         = $requestData['form_id'];
                $insertField['name']            = $requestData['field_name'];
                $insertField['field_value']     = !empty($requestData['field_value']) ? $requestData['field_value'] : '';
                $insertField['placeholder']     = !empty($requestData['placeholder']) ? $requestData['placeholder'] : '';
                $insertField['field_id']        = !empty($requestData['field_id']) ? $requestData['field_id'] : '';
                $insertField['field_classes']   = !empty($requestData['field_classes']) ? $requestData['field_classes'] : '';
                $insertField['label']           = $requestData['field_label'];
                $insertField['type']            = $requestData['field_type'];
                $insertField['attributes']      = !empty($requestData['attributes']) ? $requestData['attributes'] : '';
                $insertField['db_mapping']      = $requestData['db_mapping'];
                $insertField['list_code']       = !empty($requestData['list_code']) ? $requestData['list_code'] : '';
                $insertField['child_dd_id']     = !empty($requestData['child_dd_id']) ? implode(",",$requestData['child_dd_id']) : '';
                $insertField['sequence']        = $requestData['sequence'];

                //dd($insertField);
                $masterModel->insertData($insertField, 'form_fields');
            }

        } catch (\Illuminate\Database\QueryException $ex) {
            // $common     = new Common();
            // $common->error_logging($ex, 'store', 'FormSettingController.php');
            // return view('errors.oh!');
            dd($ex->getMessage());
            dd($ex->getLine());
        }
        // return $insertField;
        return redirect('form_setting/edit?id=' . $requestData['form_id'])->with('success', 'Field Created successfully');
    }

    /*
     @ Edit Form Field.
     */
    public function editFormField(Request $request)
    {
        $requestData = $request->all();
        $masterModel = new MasterModel();

        try {
            if($requestData['filter_type'] == 'filter') {
                $where['id'] = $requestData['field_id'];
                $fieldDetails = $masterModel->getMasterRow('form_fields', $where);
            }else if ($requestData['filter_type'] == 'group'){
                $where['id'] = $requestData['field_id'];
                $fieldDetails = $masterModel->getMasterRow('report_attribute', $where);
            }
            // dd($fieldDetails);
            return json_encode($fieldDetails);
        } catch (\Illuminate\Database\QueryException $ex) {
            // $common     = new Common();
            // $common->error_logging($ex, 'editFormField', 'FormSettingController.php');
            // return view('errors.oh!');
            dd($ex->getMessage());
            dd($ex->getLine());
        }
    }

    /*
     @ Delete Form Field.
     */
    public function deleteFormField(Request $request)
    {
        $requestData = $request->all();
        // dd($requestData);
        $masterModel = new MasterModel();

        try {
            if(!empty($requestData['field_id'])){
                $data['is_deleted'] = 1;
                $where['id'] = $requestData['field_id'];
                $query = $masterModel->updateData($data, 'form_fields', $where);

                return;
            }

        } catch (\Illuminate\Database\QueryException $ex) {
            // $common     = new Common();
            // $common->error_logging($ex, 'deleteFormField', 'FormSettingController.php');
            // return view('errors.oh!');
            dd($ex->getMessage());
            dd($ex->getLine());
        }
    }

    /*
     @ Restore Form Field.
     */
    public function restoreFormField(Request $request)
    {
        $requestData = $request->all();
        $masterModel = new MasterModel();

        try {
            if(!empty($requestData['field_id'])){
                $data['is_deleted'] = 0;
                $where['id'] = $requestData['field_id'];
                $query = $masterModel->updateData($data, 'form_fields', $where);

                return;
            }

        } catch (\Illuminate\Database\QueryException $ex) {
            // $common     = new Common();
            // $common->error_logging($ex, 'deleteFormField', 'FormSettingController.php');
            // return view('errors.oh!');
            dd($ex->getMessage());
            dd($ex->getLine());
        }
    }

    /*
    @ Store form field.
    */
    public function updateField(Request $request){
        $requestData = $request->all();
        $masterModel = new MasterModel();
        // dd($requestData);
        try {
            if($requestData['filter_type'] == 'filter') {

                $updateField['form_id']         = $requestData['form_id'];
                $updateField['name']            = $requestData['edit_field_name'];
                $updateField['field_value']     = !empty($requestData['edit_field_value']) ? $requestData['edit_field_value'] : '';
                $updateField['placeholder']     = !empty($requestData['edit_placeholder']) ? $requestData['edit_placeholder'] : '';
                $updateField['field_id']        = !empty($requestData['edit_field_id']) ? $requestData['edit_field_id'] : '';
                $updateField['field_classes']   = !empty($requestData['edit_field_classes']) ? $requestData['edit_field_classes'] : '';
                $updateField['label']           = $requestData['edit_field_label'];
                $updateField['type']            = $requestData['edit_field_type'];
                $updateField['attributes']      = !empty($requestData['edit_attributes']) ? $requestData['edit_attributes'] : '';
                $updateField['db_mapping']      = $requestData['edit_db_mapping'];
                $updateField['list_code']       = !empty($requestData['edit_list_code']) ? $requestData['edit_list_code'] : '';
                $updateField['child_dd_id']     = !empty($requestData['edit_child_dd_id']) ? $requestData['edit_child_dd_id'] : '';
                $updateField['sequence']        = $requestData['edit_sequence'];
                // dd($updateField);
                $where['id'] = $requestData['field_id'];
                // dd($where);
                $masterModel->updateData($updateField, 'form_fields', $where);

            }else if ($requestData['filter_type'] == 'group'){
                $updateField['attribute_name']            = $requestData['edit_attribute_name'];

                $where['id']                    = $requestData['attribute_id'];
                $masterModel->updateData($updateField, 'report_attribute', $where);
            }
        } catch (\Illuminate\Database\QueryException $ex) {
            // $common     = new Common();
            // $common->error_logging($ex, 'updateField', 'FormSettingController.php');
            // return view('errors.oh!');
            dd($ex->getMessage());
            dd($ex->getLine());
        }
        return redirect('forms/edit?id=' . $requestData['form_id'])->with('success', 'Field Updated successfully');
    }
}
