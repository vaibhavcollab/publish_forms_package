<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use  Illuminate\Support\Facades\DB;
class FormFieldsAfterUpdateTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::unprepared("CREATE TRIGGER form_fields_after_update_trigger AFTER UPDATE ON form_fields FOR EACH ROW

        BEGIN
  DECLARE GM_STATUS VARCHAR(20);
  IF NEW.form_id <> OLD.form_id         THEN
    SET GM_STATUS = insert_general_master_log('form_fields','form_id',old.form_id,new.form_id,CURRENT_USER,old.id,'192.168.43.1','W','0','0');
  ELSEIF  NEW.name <> OLD.name   THEN
    SET GM_STATUS = insert_general_master_log('form_fields','name',old.name,new.name,CURRENT_USER,old.id,'192.168.43.1','W','0','0');
  ELSEIF NEW.field_value <> OLD.field_value     THEN
    SET GM_STATUS = insert_general_master_log('form_fields','field_value',old.field_value,new.field_value,CURRENT_USER,old.id,'192.168.43.1','W','0','0');
  ELSEIF NEW.placeholder <> OLD.placeholder       THEN
    SET GM_STATUS = insert_general_master_log('form_fields','placeholder',old.placeholder,new.placeholder,CURRENT_USER,old.id,'192.168.43.1','W','0','0');
  ELSEIF NEW.field_id <> OLD.field_id THEN
    SET GM_STATUS = insert_general_master_log('form_fields','field_id',old.field_id,new.field_id,CURRENT_USER,old.id,'192.168.43.1','W','0','0');
ELSEIF NEW.label <> OLD.label THEN
    SET GM_STATUS = insert_general_master_log('form_fields','label',old.label,new.label,CURRENT_USER,old.id,'192.168.43.1','W','0','0');
ELSEIF NEW.type <> OLD.type THEN
    SET GM_STATUS = insert_general_master_log('form_fields','type',old.type,new.type,CURRENT_USER,old.id,'192.168.43.1','W','0','0');
ELSEIF NEW.attributes <> OLD.attributes THEN
    SET GM_STATUS = insert_general_master_log('form_fields','attributes',old.attributes,new.attributes,CURRENT_USER,old.id,'192.168.43.1','W','0','0');
ELSEIF NEW.db_mapping <> OLD.db_mapping THEN
    SET GM_STATUS = insert_general_master_log('form_fields','db_mapping',old.db_mapping,new.db_mapping,CURRENT_USER,old.id,'192.168.43.1','W','0','0');
ELSEIF NEW.list_code <> OLD.list_code THEN
    SET GM_STATUS = insert_general_master_log('form_fields','list_code',old.list_code,new.list_code,CURRENT_USER,old.id,'192.168.43.1','W','0','0');
ELSEIF NEW.child_dd_id <> OLD.child_dd_id THEN
    SET GM_STATUS = insert_general_master_log('form_fields','child_dd_id',old.child_dd_id,new.child_dd_id,CURRENT_USER,old.id,'192.168.43.1','W','0','0');
ELSEIF NEW.sequence <> OLD.sequence THEN
    SET GM_STATUS = insert_general_master_log('form_fields','sequence',old.sequence,new.sequence,CURRENT_USER,old.id,'192.168.43.1','W','0','0');
ELSEIF NEW.parent_id <> OLD.parent_id THEN
    SET GM_STATUS = insert_general_master_log('form_fields','parent_id',old.parent_id,new.parent_id,CURRENT_USER,old.id,'192.168.43.1','W','0','0');
  ELSEIF NEW.is_deleted <> OLD.is_deleted       THEN
    SET GM_STATUS = insert_general_master_log('form_fields','is_deleted',old.is_deleted,new.is_deleted,CURRENT_USER,old.id,'192.168.43.1','W','0','0');
  END IF;
END

        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("DROP TRIGGER 'form_fields_after_update_trigger'");
    }
}
