<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['namespace' => 'App\packages\forms\src\Controllers', /*'prefix' => 'form',*/ 'middleware' => ['web']], function() {

   /* Form Setting Form-Form-Field */
Route::resource('form_setting', 'FormSettingController');
Route::get('forms/edit', 'FormSettingController@show');
Route::post('editFormField', 'FormSettingController@editFormField');
Route::post('deleteFormField', 'FormSettingController@deleteFormField');
Route::post('restoreFormField', 'FormSettingController@restoreFormField');
Route::post('forms/update', 'FormSettingController@updateField');

Route::post('form_setting/update', 'FormSettingController@updateField');

    /*to generate form and save form data */
Route::get('generate_form', 'FormsController@index');
Route::post('generic_save', 'FormsController@genericSave');
});


