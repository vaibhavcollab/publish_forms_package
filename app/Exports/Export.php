<?php

namespace App\Exports;

use App\common\Common;
use App\User;
use Exception;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class Export implements FromCollection, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        if (session()->has('query')) {
            $query = session()->get('query');
            DB::statement(DB::raw('set @rownum=0'));
            $data = DB::select(DB::raw($query));

            foreach ($data as $key => $value) {
                foreach ($value as $f_key => $f_val) {
                    if (strpos($f_key, '_style') !== false) {
                        unset($value->$f_key);
                    }
                    if (strpos($f_key, '_tool_tip') !== false) {
                        unset($value->$f_key);
                    }
                    if (strpos($f_key, 'encrypted_id') !== false) {
                        unset($value->$f_key);
                    }
                    if (strpos($f_key, '_link') !== false) {
                        unset($value->$f_key);
                    }
                }
            }

            // session()->remove('query');
            //$this->removeExcelSessionValue();
            return collect($data);
        } else {
            echo "Please refresh the page before exporting.";
            die;
        }
    }

    public function headings(): array
    {
        if (session()->has('headers')) {
            return session()->get('headers');
        } else {
            echo "<h3 class='text-center text-primary'>Please Refresh the Page before Export</h3>";
            die;
        }
    }

    /*
     * $sql is your sql query.
     * $headers is your Header in Excel File.
     * $parameter is your binding parameter to the sql.
     */

    //public function setExcelParameters($sql,$headers,$parameters){
    public function setExcelParameters($sql, $headers, $page_title)
    {
        //   $this->removeExcelSessionValue();
        session(['query' => $sql]);
        session(['headers' => $headers]);
        session(['page_title' => $page_title]);
        session(['currentRoute' => \Illuminate\Support\Facades\Route::currentRouteName()]);
        /*echo "<pre>";
        print_r($sql);
        die;*/
    }
    public function removeExcelSessionValue()
    {
        session()->remove('query');
        session()->remove('headers');
        session()->remove('page_title');
        session()->remove('currentRoute');
    }
    public static function eloquentSqlWithBindings($queryBuilder)
    {
        $sql = str_replace('?', '%s', $queryBuilder->toSql());

        $handledBindings = array_map(function ($binding) {
            if (is_numeric($binding)) {
                return $binding;
            }

            if (is_bool($binding)) {
                return ($binding) ? 'true' : 'false';
            }

            return "'{$binding}'";
        }, $queryBuilder->getBindings());

        return vsprintf($sql, $handledBindings);
    }
}
